<?php

use Illuminate\Support\Facades\DB;

class ForwardConverter
{
    const rulesMain = [
        "\\\\f+[^\;]+;",
        "\\\\F+[^\;]+;",
        "\\\\p+[^\;]+;",
        "\\\\W+[^\;]+;",
        "\\\\H+[^\;]+;",
        "\\\\C+[^\;]+;",
        "\\\\A+[^\;]+;",
        "\\\\S+[^\;]+;",
        "\\\\T+[^\;]+;",
        "\\\\A+[^\;]+;",
        "\\\\P",
        "\\\\L",
        "\\\\l",
        "\^+J",
        '{[^;]+;',
        '\^+I',
        '\}',
        '%%C',
        '%%U',
    ];

    const limitVal = 20;

    private $rulesAll;
    private $rulesSoft;
    private $rulesMedium;
    private $rulesHard;

    public function __construct($request)
    {
        $this->rulesAll = $this->getRules($request->ignore);
        //когда скрывается только часть текста
        $this->rulesSoft   = isset($this->rulesAll[1]) ? array_merge(self::rulesMain, $this->rulesAll[1]) : self::rulesMain;
        //скрывается весь сегмент, если есть вхождение
        $this->rulesMedium = isset($this->rulesAll[2]) ? $this->rulesAll[2] : null;
        //скрывается если сегмент полностью соотвествует игнору
        $this->rulesHard   = isset($this->rulesAll[3]) ? $this->rulesAll[3] : null;
    }

    /**
     * Прямой конвертер
     * @param  [array]      $request
     * @param  [string]     $name      [название файла]
     * @param  [string]     $extension [расширение файла]
     * @param  [bool]       $iVal      [условие для демо-режима]
     * @return none
     */
    public function start($request, $name, $iVal, $fileId)
    {
        $archive = $request->archive;
        $extension = 'dxf';
        $file = 'uploaded/'.$archive.'/'.$name.'.'.$extension;
        $newFile = 'uploaded/'.$archive.'/'.$name.'_fort.'.$extension;

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $fontStyleName = 'hiddenStyle';
        $phpWord->addFontStyle($fontStyleName, array('hidden' => true, 'bgColor' => '00ffb8'));
        $textFileArrayFull = '';
        $textFileArrayFull = Storage::get($file);
        $nameFile = $name;
        $i = 1;
        $chunks = [];

        $textFileArrayFull = explode("AcDbEntity", $textFileArrayFull);
        $chunks = collect($textFileArrayFull)->chunk(200);
        $newFileDxf = fopen(storage_path('app/'.$newFile), 'w');
        foreach ($chunks as $chunkKey => $chunk) {
            foreach ($chunk as $keyChunk => $textFileArray) {
                $part = explode("\r\n", $textFileArray);
                $part = collect($part);
                if($keyChunk != 0) {
                    $part->put(0, 'AcDbEntity');
                }
                if(!$iVal && $i > self::limitVal) {
                    fwrite($newFileDxf, implode("\r\n", $part->toArray()));
                    unset($part);
                    continue;
                }
                if($request->layers) {
                    $chekLayer = $this->chekLayer($part->take(20)->toArray(), $request->layers);
                } else {
                    $chekLayer = 1;
                }
                if($chekLayer == 1) {
                    $keyAcDbText = $part->search('AcDbText');
                    if($keyAcDbText) {
                        $haveLine = $this->getEasyText($i, $part, $section, $fontStyleName, 'AcDbText');
                        if($haveLine) {
                            $i++;
                        }
                    }
                    $keyAcDbMText = $part->search('AcDbMLeader');
                    if($keyAcDbMText) {
                        $haveLine = $this->getLeaderText($i, $part, $section, $fontStyleName, 'AcDbMLeader');
                        if($haveLine) {
                            $i++;
                        }
                    }
                    $keyAcDbTable = $part->search('AcDbTable');
                    if($keyAcDbTable) {
                        $i = $this->getTableText($i, $part, $section, $fontStyleName, 'AcDbTable', $iVal);
                    }
                }
                fwrite($newFileDxf, implode("\r\n", $part->toArray()));
                unset($part);
            }
        }
        fclose($newFileDxf);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(storage_path('app/uploaded/'.$archive.'/'.$nameFile.'.docx'));
        unset($objWriter);
        unset($phpWord);
        Storage::delete($file);
        Storage::move($newFile, $file);
        DB::table('attachments')->whereId($fileId)->update(['done' => 1]);
    }

    /**
     * Проверка на атрибуты
     * @param  [array] $part [description]
     * @return [numeric]       [Возвращаем число]
     */
    private function checkAttribute($part)
    {
        $acDbAttribute = 0;
        $flags = null;
        foreach ($part as $chVal) {
            $posAb = strpos($chVal, 'AcDbAttribute');
            if ($posAb > -1) {
                $flags =  collect($part)->search(' 70');
                if($flags && $part[$flags+1] == '     1') {
                    $acDbAttribute++;
                }
            }
        }
        return $acDbAttribute;
    }

    /**
     * Проверка игнора
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    private function checkInIgnore($value)
    {
        $kPos = 0;
        $value = preg_replace('!\s+!', ' ', $value);
        if($this->rulesMedium) {
            foreach ($this->rulesMedium as $ignore) {
                $pos = strpos($value, $ignore);
                if ($pos > -1) {
                    $kPos++;
                }
            }
        }
        if($this->rulesHard) {
            $value = preg_replace("/".implode('|', self::rulesMain)."/", '', $value);
            foreach ($this->rulesHard as $ignore) {
                if ($value == $ignore) {
                    $kPos++;
                }
            }
        }
        return $kPos;
    }
    
    /**
     * Проверка на вхожение слоя
     * @param  [type] $startPart [description]
     * @return [type]            [description]
     */
    private function chekLayer($startPart, $layers)
    {
        $pos = array_search('  8', $startPart);
        $ch =  collect($layers)->search($startPart[$pos+1]);
        if($ch > -1) {
            return true;
        }
    }

    /**
     * Получаем обычный текст
     * @param  [type] $i             [description]
     * @param  [type] $part          [description]
     * @param  [type] $section       [description]
     * @param  [type] $fontStyleName [description]
     * @param  [type] $type          [description]
     * @return [type]                [description]
     */
    private function getEasyText($i, $part, $section, $fontStyleName, $type)
    {
        $mtextArray = [];
        $checkAttribute = $this->checkAttribute($part);
        foreach ($part as $index => $partValue) {
            if($partValue === '  3' && $type == 'AcDbMText' || $partValue === '  1' && $checkAttribute == 0) {
                $value = $part[$index+1];
                $a = preg_replace("/".implode('|', self::rulesMain)."/", '', $value);
                $a = preg_replace('!\s+!', ' ', $a);
                if(!empty(trim($a)) && !is_numeric(trim($a))) {
                    $mtextArray[$index+1]['value'] = $value;
                    $part[$index+1] = '';
                }
                unset($a);
            }
            if($partValue === 'ENDSEC') {
                break;
            }
        }
        $mtextArray = collect($mtextArray);
        if($mtextArray->isNotEmpty()) {
            $firstKey = $mtextArray->keys()->first();
            $fullLine = $mtextArray->implode('value', '');
            $textTag = $this->passText($i, $fullLine, $fontStyleName, $section);
            $part[$firstKey] = $textTag;
            return true;
        }
        unset($mtextArray);
        return false;
    }

    /**
     * Получаем текст со сносок
     * @param  [type] $i             [description]
     * @param  [type] $part          [description]
     * @param  [type] $section       [description]
     * @param  [type] $fontStyleName [description]
     * @param  [type] $type          [description]
     * @return [type]                [description]
     */
    private function getLeaderText($i, $part, $section, $fontStyleName, $type)
    {
        $mtextArray = [];
        foreach ($part as $index => $partValue) {
            if($partValue === '304') {
                $value = $part[$index+1];
                if(!empty($value) && !is_numeric(trim($value))) {
                    $mtextArray[$index+1]['value'] = $value;
                    $part[$index+1] = '';
                }
            }
            if($partValue === ' 11') {
                break;
            }
        }
        $mtextArray = collect($mtextArray);
        if($mtextArray->isNotEmpty()) {
            $firstKey = $mtextArray->keys()->first();
            $fullLine = $mtextArray->implode('value', '');
            $textTag = $this->passText($i, $fullLine, $fontStyleName, $section);
            $part[$firstKey] = $textTag;
            return true;
        }
        unset($mtextArray);
        return false;
    }

    /**
     * Получаем текст из таблиц
     * @param  [type] $i             [description]
     * @param  [type] $part          [description]
     * @param  [type] $section       [description]
     * @param  [type] $fontStyleName [description]
     * @param  [type] $type          [description]
     * @return [type]                [description]
     */
    private function getTableText($i, $part, $section, $fontStyleName, $type, $iVal)
    {
        $cell = false;
        $valueCh = false;
        $mtextArray = [];
        foreach ($part as $index => $partValue) {
            if($partValue == 'CELL_VALUE') {
                $cell = true;
            }
            if($cell == true) {
                if($partValue === '  1' || $partValue === ' 91') {
                    $value = $part[$index+1];
                    if(isset($value) && !is_numeric(trim($value))) {
                        $mtextArray[$index+1]['value'] = $value;
                        $part[$index+1] = '';
                        $valueCh = true;
                    }
                }
                if($partValue === '302') {
                    $value302 = trim($part[$index+1]);
                    if(isset($value302) && $valueCh) {
                        $mtextArray[$index-5]['302'] = $index;
                        $part[$index+1] = '';
                    }
                }
            }
            if($partValue == 'ACVALUE_END') {
                $cell = false;
                $valueCh = false;
            }
        }
        
        $mtextArray = collect($mtextArray);
        if($mtextArray->isNotEmpty()) {
            foreach ($mtextArray as $keyMtext => $valueMtext) {
                if($i > $iVal) {
                    break;
                }
                $fullLine = $valueMtext['value'];
                $textTag = $this->passText($i, $fullLine, $fontStyleName, $section);
                $part[$keyMtext] = $textTag;
                if(isset($valueMtext['302'])) {
                    $k302 = $valueMtext['302']+1;
                    $part[$k302] = $textTag;
                }
                $i++;
            }
        }
        unset($mtextArray);
        unset($fullLine);
        return $i;
    }


    private function getRules($ignore)
    {
        $ignoreList = collect($ignore)->groupBy('type')->map(function($item, $key) {
            $ids = $item->pluck('id')->toArray();
            $words = DB::table('words')->whereIn('ignore_id', $ids)->select('text')->get();
            $words = $words->pluck('text')->map(function($word) {
                return str_replace('/', '\\/', $word);
            });
            return $words;
        })->toArray();
        return $ignoreList;
    }

    /**
     * Вставка текста в word
     * @param  [type] $i             [description]
     * @param  [type] $fullLine      [description]
     * @param  [type] $fontStyleName [description]
     * @param  [type] $section       [description]
     * @return [type]                [description]
     */
    private function passText($i, $fullLine, $fontStyleName, $section)
    {
        if($this->rulesMedium || $this->rulesHard) {
            $checkIgnore = $this->checkInIgnore($fullLine);
        } else {
            $checkIgnore = 0;
        }
        $textTag = '###'.$i.'###';
        $section->addText($textTag, $fontStyleName);
        if($checkIgnore) {
            $text = htmlspecialchars($fullLine);
            $section->addText($text, $fontStyleName);
        } else {
            preg_match_all("/".implode('|', $this->rulesSoft)."/", $fullLine, $matches);
            $split = preg_split("/".implode('|', $this->rulesSoft)."/", $fullLine);
            $textrun = $section->createTextRun();
            foreach ($split as $keySplit => $value) {
                $text = '';
                $text = htmlspecialchars($value);
                $textrun->addText($text);
                if(isset($matches[0][$keySplit])) {
                    $text = htmlspecialchars($matches[0][$keySplit]);
                    $textrun->addText($text, $fontStyleName);
                }
            }
        }
        return $textTag;
    }
}