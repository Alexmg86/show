<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Http\Requests\SupportRequest;
use App\Ignore;
use App\Mail\SupportMail;
use App\Replace;
use BackConverter;
use ForwardConverter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use JDWil\ZipStream\ZipStream;
use ReplaceConverter;

class ConverterController extends Controller
{
    
    public function uploadFiles(Request $request)
    {
        $user = Auth::user();
        $userId = null;
        $iVal = true;
        if($user) {
            $userId = $user->id;
        } else {
            $iVal = false;
        }
        $file = $request->file;
        if($file) {
            $name = uniqid();
            $native = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = strtolower($file->getClientOriginalExtension());
            $fileName = $name.'.'.$extension;
            Storage::putFileAs('uploaded/'.$request->archive, $file, $fileName);
            if($extension == 'dwg') {
                $this->convertFile($request->archive, $name, $extension);
            }
            $newFile = new Attachment([
                'user_id' => $userId, 
                'name' => $name, 
                'archive' => $request->archive, 
                'native' => $native, 
                'extension' => $extension, 
                'type' => 0,
                'done' => 0
            ]);
            $newFile->save();
            $converter = new ForwardConverter($request);
            $converter->start($request, $name, $iVal, $newFile->id);
            $this->changeBalance($request);
            return $newFile->name;
        } else if($request->name) {
            $converter = new ForwardConverter($request);
            $converter->start($request, $request->name, $iVal, $request->fileId);
            $this->changeBalance($request);
            return $request->name;
        }
    }

    /**
     * Скачиваем архив и удаляем его с сервера
     * @param  [array] $request [запрос]
     * @return [file]          [получаем архив]
     */
    public function downloadFile(Request $request)
    {
        $acthiveName = storage_path('app/archive/'.$request->name);
        return response()->download($acthiveName)->deleteFileAfterSend(true);
    }

    /**
     * Создаем арзив для одного файла
     * @param  [array] $request [запрос]
     * @return [string]         [название архива]
     */
    public function makeArchive(Request $request)
    {
        $files = Attachment::where(['name' => $request->id, 'done' => 1])->get();
        return $this->makeZip($files, $files->first()->archive);
    }

    /**
     * Создаем арзив для всех файлов
     * @param  [array] $request [запрос]
     * @return [string]         [название архива]
     */
    public function makeArchives(Request $request)
    {
        $files = Attachment::where(['archive' => $request->archive, 'done' => 1])->get();
        return $this->makeZip($files, $request->archive);
    }

    /**
     * Создаем архив с файлами
     * @param  [type] $files   [массив файлов]
     * @param  [type] $archive [название сессии]
     * @return [type]          [description]
     */
    public function makeZip($files, $archive)
    {
        $acthiveNameShort = $archive .'_'.\Carbon\Carbon::now()->micro.'.zip';
        $acthiveName = storage_path('app/archive/'.$acthiveNameShort);
        $zip = ZipStream::forFile($acthiveName);
        foreach ($files as $file) {
            $arExt = [$file->extension];
            if($file->type == 0) {
                $arExt = ['dxf', 'docx'];
            } else if ($file->type == 1 && strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                $arExt = ['dwg'];
            }
            foreach ($arExt as $ext) {
                $fileNameOld = $file->archive.'/'.$file->name . '.' . $ext;
                $fileNameNew = $file->native . '.' . $ext;
                $exist = Storage::exists('uploaded/'.$fileNameOld);
                if($exist) {
                    $zip->addFileFromDisk($fileNameNew, storage_path('app/uploaded/'.$fileNameOld));
                }
            }
        }
        $zip->close();
        return $acthiveNameShort;
    }

    public function convertFile($archive, $name, $extension)
    {
        $dwg = storage_path("app/uploaded/$archive/$name.dwg");
        $dxf = storage_path("app/uploaded/$archive/$name.dxf");
        $path = config('prices.path');
        if($extension == 'dwg') {
            $version = $this->checkVersionDwg($dwg);
            exec("$path /r /x$version $dwg $dxf");
        } else if($extension == 'dxf') {
            $version = $this->chekVersionDxf($dxf);
            exec("$path /r /g$version $dxf $dwg");
        }
    }

    private function chekVersionDxf($file)
    {
        $versions = $this->getVersion();
        $handle = fopen($file, 'r');
        $i = 1;
        if($handle) {
            while(!feof($handle)) {
                $text = fgets($handle);
                if($text == "\$ACADVER\r\n") {
                    $version = $i;
                }
                if(isset($version) && $i == $version + 2) {
                    break;
                }
                $i++;
            }
            fclose($handle);
        }
        $text = str_replace("\r\n", '', $text);
        return isset($versions[$version]) ? $versions[$version] : '2007';
    }

    private function checkVersionDwg($file)
    {
        $versions = $this->getVersion();
        $handle = fopen($file, 'r');
        if($handle) {
            $version = fgets($handle, 7);
            fclose($handle);
        }
        return isset($versions[$version]) ? $versions[$version] : '2007';
    }

    private function getVersion()
    {
        return [
            // 'AC1002' => '25',
            // 'AC1003' => '26',
            // 'AC1004' => '9',
            // 'AC1006' => '10',
            // 'AC1009' => '11',
            // 'AC1012' => '13',
            // 'AC1014' => '14',
            // 'AC1015' => '2000',
            // 'AC1018' => '2004',
            'AC1021' => '2007',
            'AC1024' => '2010',
            'AC1027' => '2013',
            'AC1032' => '2013'
        ];
    }

    /**
     * Страница обратной связи
     */
    public function support()
    {
        return view('layouts.support');
    }

    /**
     * Отправка письма
     */
    public function calltosupport(SupportRequest $request)
    {
        Mail::to('alexmg86@mail.ru')->send(new SupportMail($request->all()));
        return redirect()->back()->with('success', "Your message was successfully sent.");
    }
}
